# -- Path setup --------------------------------------------------------------
import os
from pathlib import Path

import sass
from sphinx.application import Sphinx
from sphinx.ext.todo import todo_node, visit_todo_node, depart_todo_node
from sphinxcontrib.blockdiag import (
    blockdiag_node,
    html_visit_blockdiag,
    html_depart_blockdiag,
)

# -- Prepared proc -----------------------------------------------------------
here = Path(__file__).parent

# -- Project information -----------------------------------------------------
project = 'July Tech Festa 2021'
copyright = '2021, Kazuya Takei'
author = 'Kazuya Takei'

# The full version, including alpha/beta/rc tags
release = "2021.6"

# -- General configuration ---------------------------------------------------
language = 'ja'
extensions = [
    "sphinx.ext.todo",
    "sphinxcontrib.blockdiag",
    "sphinxcontrib.gtagjs",
    "sphinxcontrib.sass",
    "sphinxcontrib.twitter",
    "sphinx_revealjs",
    "sphinxemoji.sphinxemoji",
]
templates_path = ["_templates"]
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', '_sections', ".venv", "var", 'dist', 'demo']

# -- Options for HTML output -------------------------------------------------
html_theme = 'alabaster'
html_static_path = ['_static']

# -- Options for HTML output -------------------------------------------------
revealjs_static_path = html_static_path
revealjs_static_path = [
    "_static",
]
revealjs_css_files = [
    "https://fonts.googleapis.com/css2?family=M+PLUS+1p&amp;display=swap",
    "css/custom.css",
    "revealjs4/plugin/highlight/zenburn.css",
]
revealjs_style_theme = "solarized"
revealjs_script_plugins = [
    {
        "name": "RevealNotes",
        "src": "revealjs4/plugin/notes/notes.js",
    },
    {
        "name": "RevealHighlight",
        "src": "revealjs4/plugin/highlight/highlight.js",
    },
]
revealjs_script_conf = """
{
    hash: true,
    center: false,
    transition: 'none',
}
"""

# -- Extention configurations ----------
# sphinx.ext.todo
todo_include_todos = True

# sphinxcontrib.blockdiag
blockdiag_html_image_format = "SVG"

# sphinxcontrib.gtagjs
gtagjs_ids = []
if "GTAGJS_IDS" in os.environ:
    gtagjs_ids = os.environ["GTAGJS_IDS"].split(",")

# sphinxcontrib.sass
sass_out_dir = "_static/css"
sass_targets = {
    "_sass/custom.scss": "_static/css/custom.css",
}
