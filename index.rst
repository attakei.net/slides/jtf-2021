======================================
「〇〇のプラグインを作る」ことのすゝめ
======================================

:Date: 2021/7/18
:Author: `Kazuya Takei <https://attakei.net>`_
:Event: `July Tech Festa 2021 <https://techfesta.connpass.com/event/213069/>`_
:Hashtag: `#JTF2021_a <https://twitter.com/hashtag/JTF2021_a>`_

.. .. include:: _sections/0-pdf-edition.rst

.. include:: _sections/1-introduction.rst

.. include:: _sections/2-about-plugin.rst

.. include:: _sections/3-plugin-as-oss.rst

.. include:: _sections/4-develop-plugin.rst

.. include:: _sections/5-cautions.rst

.. include:: _sections/6-conclusion.rst
