FROM python:3.9-slim as build
ARG GTAGJS_IDS

RUN mkdir -p /workspace
COPY ./ /workspace
WORKDIR /workspace
RUN pip install poetry \
    && poetry install \
    && poetry run sphinx-build -b revealjs /workspace/ /workspace/dist

FROM nginx:stable-alpine
ARG SLIDE_NAME
COPY --from=build /workspace/dist/ /usr/share/nginx/html
RUN mkdir -p /var/www/html && { \
    echo "User-agent: *"; \
    echo "Disallow: /"; \
} > /var/www/html/robots.txt;
RUN { \
    echo "server {"; \
    echo "  listen      8080;"; \
    echo "  server_name slide-server;"; \
    echo "  root /var/www/html;"; \
    echo "  location /slides/$SLIDE_NAME {"; \
    echo "    alias /usr/share/nginx/html;"; \
    echo "    index index.html;"; \
    echo "  }"; \
    echo "  error_page 500 502 503 504 /50x.html;"; \
    echo "  location = /50x.html {"; \
    echo "    root /usr/share/nginx/html;"; \
    echo "  }"; \
    echo "}"; \
} > /etc/nginx/conf.d/default.conf
